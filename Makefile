CC = gcc
CFLAGS = -I. -lm

all:
	$(CC) -o programa main.c mask.c $(CFLAGS)

clean:
	rm -f programa
	rm -f *.o