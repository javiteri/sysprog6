#include "mask.h"
#include <stdio.h>

static int mask_owner, mask_group, mask_other;

void clean_stdin(void);

int main() {
	int mascaraRealOwner, mascaraRealGroup, mascaraRealAll;
	mascaraRealOwner = mascaraRealGroup = mascaraRealAll = 0;
	int *mascaraPunteroOwner , *mascaraPunteroGroup , *mascaraPunteroAll ;
	mascaraPunteroOwner = &mascaraRealOwner;
	mascaraPunteroAll = &mascaraRealAll;
	mascaraPunteroGroup = &mascaraRealGroup;
	char charLecturaOwner, charEscrituraOwner, charEjecucionOwner , charLecturaGroup , charEscrituraGroup, charEjecucionGroup , charLecturaAll , charEscrituraAll , charEjecucionAll;

	//PARTE OWNER
	printf("1. Permiso lectura para mask_owner? [s/n]\n");
	scanf("%c", &charLecturaOwner);
	clean_stdin();
	if ( charLecturaOwner == 's'){
		//printf("entra aqui\n");
		setPermiso( mascaraPunteroOwner , 'r', SET);
	}else{
		setPermiso( mascaraPunteroOwner , 'r', UNSET);
	}
	//printf("Este es charLecturaOwner %c\n", charLecturaOwner );

	printf("2. Permiso escritura para mask_owner? [s/n]\n");
	scanf("%c", &charEscrituraOwner);
	clean_stdin();
	if ( charEscrituraOwner == 's'){
		setPermiso( mascaraPunteroOwner , 'w', SET);
	}else{
		setPermiso( mascaraPunteroOwner , 'w', UNSET);
	}
	//printf("Este es charEscrituraOwner %c\n", charEscrituraOwner );

	printf("3. Permiso ejecucion para mask_owner? [s/n]\n");
	scanf("%c", &charEjecucionOwner);
	clean_stdin();	
	if ( charEjecucionOwner == 's'){
		setPermiso( mascaraPunteroOwner , 'x', SET);
	}else{
		setPermiso( mascaraPunteroOwner , 'x', UNSET);
	}
	//printf("Este es charEjecucionOwner %c\n", charEjecucionOwner );
	
	/*
	printf("--------------------------------\n");
	printf("Este es el valor de la máscara del owner: %d\n", mascaraRealOwner);
	printf("--------------------------------\n");
	*/

	//PARTE GROUP
	printf("4. Permiso lectura para mask_group? [s/n]\n");
	scanf("%c", &charLecturaGroup);
	clean_stdin();
	if ( charLecturaGroup == 's'){
		//printf("entra aqui\n");
		setPermiso( mascaraPunteroGroup , 'r', SET);
	}else{
		setPermiso( mascaraPunteroGroup , 'r', UNSET);
	}
	//printf("Este es charLecturaGroup %c\n", charLecturaGroup );

	printf("5. Permiso escritura para mask_group? [s/n]\n");
	scanf("%c", &charEscrituraGroup);
	clean_stdin();
	if ( charEscrituraGroup == 's'){
		setPermiso( mascaraPunteroGroup , 'w', SET);
	}else{
		setPermiso( mascaraPunteroGroup , 'w', UNSET);
	}
	//printf("Este es charEscrituraGroup %c\n", charEscrituraGroup );

	printf("6. Permiso ejecucion para mask_group? [s/n]\n");
	scanf("%c", &charEjecucionGroup);
	clean_stdin();
	if ( charEjecucionGroup == 's'){
		setPermiso( mascaraPunteroGroup , 'x', SET);
	}else{
		setPermiso( mascaraPunteroGroup , 'x', UNSET);
	}
	//printf("Este es charEjecucionGroup %c\n", charEjecucionGroup );
	/*
	printf("--------------------------------\n");
	printf("Este es el valor de la máscara del grupo: %d\n", mascaraRealGroup);
	printf("--------------------------------\n");
	*/

	//PARTE ALL
	printf("7. Permiso lectura para mask_all? [s/n]\n");
	scanf("%c", &charLecturaAll);
	clean_stdin();
	if ( charLecturaAll == 's'){
		//printf("entra aqui\n");
		setPermiso( mascaraPunteroAll , 'r', SET);
	}else{
		setPermiso( mascaraPunteroAll , 'r', UNSET);
	}
	//printf("Este es charLecturaAll %c\n", charLecturaAll );

	printf("8. Permiso escritura para mask_all? [s/n]\n");
	scanf("%c", &charEscrituraAll);
	clean_stdin();
	if ( charEscrituraAll == 's'){
		setPermiso( mascaraPunteroAll , 'w', SET);
	}else{
		setPermiso( mascaraPunteroAll , 'w', UNSET);
	}
	//printf("Este es charEscrituraAll %c\n", charEscrituraAll );

	printf("9. Permiso ejecucion para mask_all? [s/n]\n");
	scanf("%c", &charEjecucionAll);
	clean_stdin();
	if ( charEjecucionOwner == 's'){
		setPermiso( mascaraPunteroAll , 'x', SET);
	}else{
		setPermiso( mascaraPunteroAll , 'x', UNSET);
	}
	//printf("Este es charEjecucionOwner %c\n", charEjecucionAll );
	/*
	printf("--------------------------------\n");
	printf("Este es el valor de la máscara de All: %d\n", mascaraRealAll);
	printf("--------------------------------\n");
	*/
	printf("--------------------------------\n");
	printf("Este es el valor de la mascara de permisos %d%d%d \n", mascaraRealOwner, mascaraRealGroup , mascaraRealAll);
	printf("--------------------------------\n");

	printf("Presione ENTER para terminar\n");  
	getchar();

}

void clean_stdin(void) {
	//printf("Se va a limpiar el buffer.\n");
	int c;
 	do {
     c = getchar();
	}while (c != '\n' && c != EOF);
}