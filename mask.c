#include "mask.h"
#include <stdio.h>

/*
El parámetro mascara es la máscara que se debe mantener para cada nivel de acceso y debe de ser enviado por referencia. El parámetro permiso es un char con los valores r, w, o x y set es un boolean con valores de  0 o 1. Los valores de 0 y 1 deben de estar definidos con un #define como SET y UNSET. 
*/
void setPermiso(int *mascara, char permiso, int set){
	switch (permiso){
		case 'r' :
			*mascara |= (set << 2);
			//printf("%d esta es mascara\n", *mascara );
			break;
		case 'w' :
			*mascara |= (set << 1);
			break;

		case 'x' :
			*mascara |= (set);
			break;
	}
	//printf("%d esta es mascara\n", *mascara );

}
